const express = require('express');
const router = express.Router();

router.route('/member/edit/:id')
    .all((req, res, next)=>{
        res.locals.myData = '567';
        next();
    })
    .get((req, res)=>{
        res.json({
            baseUrl: req.baseUrl,
            url: req.url,
            myData: res.locals.myData
        });
    })
    .post((req, res)=>{
        res.send(res.locals.myData);
    });

export default router;
export { router};



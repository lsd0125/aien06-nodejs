import express from 'express';
import url from 'url';
import bodyParser from 'body-parser';
import multer from 'multer';
const upload = multer({dest: 'tmp_uploads/'});
// const fs = require('fs');
import fs from 'fs';
import session from 'express-session';
import moment from 'moment-timezone';
// const db from __dirname + '/db-connect');

express.howareyou = 'fine';

const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));
//app.use(bodyParser.json());

app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'ksdhkasjhfjs',
    cookie: {
        maxAge: 1200000
    }
}));



app.get('/', (req, res)=>{
    res.render('home', {
        name: '<Tom>',
        // title: 'Home'
    });
    //res.send('hello');
});

app.get('/sales', (req, res)=>{
    const data = require(__dirname+'/../data/sales');
    res.render('sales', {
        sales: data,
        title: 'Sales list'
    });
    //res.json(data);
});

app.get('/try-qs', (req, res)=>{
    const urlParts = url.parse(req.url, true);
    res.json(urlParts);
});

app.post(
    '/try-post',
    //bodyParser.urlencoded({extended: false}),
    (req, res)=>{
        res.json(req.body);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});
app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', {
        data: req.body,
        a: 12
    });
});

app.get('/try-ajax', (req, res)=>{
    res.render('try-ajax');
});
app.post('/try-ajax', (req, res)=>{
    res.json(req.body);
});

app.get('/try-upload', (req, res)=>{
    res.render('try-upload');
});
app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    // console.log(req.file);
    // res.json(req.file);
    let result = {
        success: false,
        name: '',
        avatar: '',
    };

    if(req.file && req.file.originalname){
        switch(req.file.mimetype){
            case 'image/jpeg':
            case 'image/png':
                fs.createReadStream(req.file.path)
                    .pipe(fs.createWriteStream('public/img/'+req.file.originalname));
                result = {
                    success: true,
                    name: req.body.name,
                    avatar: '/img/'+req.file.originalname,
                };
                break;
            default:
        }
    }
    res.render('try-upload', result);

});

app.get('/my-params1/:action?/:sid?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/09\d{2}\-?\d{3}\-?\d{3}$/, (req, res)=>{
    let u = req.url.slice(1);
    u = u.split('?')[0];
    u = u.split('-').join('');
    res.send(u);
});

const admin1 = require(__dirname + '/admins/admin1');
admin1(app);

const admin2 = require(__dirname + '/admins/admin2');
app.use(admin2);

// import {router} from './admins/admin3';
// app.use('/admin3', router);
import admin3 from './admins/admin3';
app.use('/admin3', admin3);

app.get('/try-session', (req, res)=>{
    req.session.my_var = req.session.my_var || 0;
    req.session.my_var++;
    res.json({
        my_var: req.session.my_var,
        session: req.session
    });
});

app.get('/login', (req, res)=>{
    let data = {
        flashMsg: req.session.flashMsg || '',
        loginUser: req.session.loginUser
    };
    delete req.session.flashMsg;
    res.render('login', data);
});
app.post('/login', (req, res)=>{
    // SELECT * FROM `members` WHERE `email`=? AND `password`=SHA1(?)
    const list = {
      shin: '1234',
      der: '1234',
    };

    if(
        req.body.user &&
        list[req.body.user] &&
        list[req.body.user]===req.body.password
    ){
        req.session.loginUser = req.body.user;
    } else {
        req.session.flashMsg = 'Bad password';
    }
    res.redirect('/login');
});

app.get('/logout', (req, res)=>{
    delete req.session.loginUser;
    res.redirect('/login');
});

app.get('/try-moment', (req, res)=>{
    const fm = 'YYYY-MM-DD HH:mm:ss';
    const mo1 = moment(req.session.cookie.expires);
    const mo2 = moment(new Date());

    res.json({
        'local-mo1': mo1.format(fm),
        'local-mo2': mo2.format(fm),
        'london-mo1': mo1.tz('Europe/London').format(fm),
        'london-mo2': mo2.tz('Europe/London').format(fm),
    });
});
/*
app.get('/try-db/:page?', (req, res)=>{
    const perPage = 5;
    const output = {
        perPage: perPage
    };
    let page = parseInt(req.params.page) || 1;
    output.page = page;

    let sql1 = "SELECT COUNT(1) num FROM products";
    let sql2 = `SELECT * FROM products LIMIT ${(page-1)*5}, 5`;
    db.query(sql1, (error, results)=>{
        output.total = results[0].num;
        output.totalPages = Math.ceil(output.total/perPage);

        if(page<1 || page>output.totalPages){
            return res.redirect('/try-db');
        }

        db.query(sql2, (error, results)=>{
            output.rows = results;
            res.json(output);
        });

    });
});

app.get('/try-db2/:page?', (req, res)=>{
    const perPage = 5;
    const output = {
        perPage: perPage
    };
    let page = parseInt(req.params.page) || 1;
    output.page = page;

    let sql1 = "SELECT COUNT(1) num FROM products";
    let sql2 = `SELECT * FROM products LIMIT ${(page-1)*5}, 5`;

    db.queryAsync(sql1)
        .then(results=>{
            output.total = results[0].num;
            output.totalPages = Math.ceil(output.total/perPage);

            if(page<1 || page>output.totalPages){
                return res.redirect('/try-db');
            }
            return db.queryAsync(sql2);
        })
        .then(results=>{
            output.rows = results;
            res.json(output);
        })
        .catch(error=>{
            console.log(error);
        });
});

*/
// 404 要在 routes 的最後面
app.use((req, res)=>{
    res.type('text/plain');
    res.status(404);
    res.send('404 !!!!!!!!!!');
});

app.listen(3000, ()=>{
    console.log('server start');
});

// windows 查看網路 port 使用情況
// netstat -na




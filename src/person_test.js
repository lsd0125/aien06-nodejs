const Person = require('./person');

const p1 = new Person();
const p2 = new Person('Peter', 30);

console.log(p1.toJSON());
console.log(p2.toJSON());